## Kollage

> Implement a list of media items with data from my instagram account

#### Basic features

- Listing media items
- Modal functionality of a media item
    - [ ] Show appropriate media depending on its _type_
